package com.jinfang.algorithms;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class QuickSort {
	final int len = 54;
	int[] a = new int[len];

	public void print() {
		System.out.print("array a is [");
		for (int i : a) {
			System.out.print(i + ",");
		}
		System.out.println("]");
	}

	@Before
	public void init() {
		for (int i = 0; i < len; i++) {
			a[i] = i + 1;
		}
		rand();
	}

	/**
	 * 在线医生面试题 洗牌算法 将一副牌54张，打散成随机状态
	 */
	// @Test
	public void rand() {
		print();
		for (int i = 0; i < len; i++) {
			int j = new Random().nextInt(len);
			int b = 0;
			b = a[i];
			a[i] = a[j];
			a[j] = b;
		}
		System.out.println("打乱后的数组是：");
		print();
	}

	@Test
	public void sort() {
		quickSort(0, len - 1);
		System.out.println("排序后的数组是：");
		print();
	}

	public void quickSort(int from, int end) {		
		if (from < end) {
			int temp = a[from];
			int i = from, j = end;
			while (i < j) {
				while (i < j && a[j] >= temp) {
					j--;
				}
				if (i < j) {
					a[i] = a[j];
					i++;
				}
				while (i < j && a[i] <= temp) {
					i++;
				}
				if (i < j) {
					a[j] = a[i];
					j--;
				}
			}
			a[i] = temp;
			quickSort(i + 1, end);
			quickSort(from, i - 1);
		}
	}

}
