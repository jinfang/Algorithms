package com.jinfang.algorithms;

import org.junit.Test;

/**
 * 蓝鸽笔试题
 * 问题描述：查找一个文件中，某个字符串出现的次数
 * 
 * @author jinfang
 *
 */
public class StringFinder {
	String temp="ababababdsfwerwer";
	
	@Test
	public void test(){
		System.out.println(count(temp, "wer"));
	}
	
	
	public int count(String s,String key){
		int j=s.indexOf(key);
		int num=0;		
		while(j>=0){
			
			s=s.substring(j+key.length());
			j=s.indexOf(key);
			num++;
		}
		return num;
	}
	
	
}
