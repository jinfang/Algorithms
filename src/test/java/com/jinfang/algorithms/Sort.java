package com.jinfang.algorithms;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class Sort {

	final int len = 54;
	int[] a = new int[len];

	public void print() {
		System.out.print("array a is [");
		for (int i : a) {
			System.out.print(i + ",");
		}
		System.out.println("]");
	}

	@Before
	public void init() {
		for (int i = 0; i < len; i++) {
			a[i] = i + 1;
		}
		rand();
	}

	/**
	 * 在线医生面试题
	 * 洗牌算法
	 * 将一副牌54张，打散成随机状态
	 */
	// @Test
	public void rand() {
		print();
		for (int i = 0; i < len; i++) {
			int j = new Random().nextInt(len);
			int b = 0;
			b = a[i];
			a[i] = a[j];
			a[j] = b;
		}
		System.out.println("打乱后的数组是：");
		print();
	}

	/**
	 * 在线医生面试题之二
	 * 
	 * 冒泡排序算法 
	 */
//	@Test
	public void maopaoSort() {
		for (int j = 0; j < len; j++)
			for (int i = len - 1; i >= j; i--) {
				if (a[i] < a[j]) {
					int b = a[i];
					a[i] = a[j];
					a[j] = b;
				}
			}
		print();
	}
	
//	
	
	
	
}
