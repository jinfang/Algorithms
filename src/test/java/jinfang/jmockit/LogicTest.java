package jinfang.jmockit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class LogicTest extends Assert{
	Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Mocked
	Dao dao;
	
	@Tested
	Logic target;
	
	@Test
	public void testHello(){
		new Expectations() {
			{
				dao.save(anyInt);
				result=3;		
				
			}			
		};
		
		int value=target.hello(2);
		logger.info(value+"");
		assertTrue(value==3);
		
	}
	
	
}
